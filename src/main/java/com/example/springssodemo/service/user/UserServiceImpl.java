package com.example.springssodemo.service.user;

import com.example.springssodemo.dao.role.RoleDao;
import com.example.springssodemo.dao.user.UserDao;
import com.example.springssodemo.model.Role;
import com.example.springssodemo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final RoleDao roleDao;


    @Autowired
    public UserServiceImpl(UserDao userDao, RoleDao roleDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
    }


    @Override
    public User getUser(long id) {
        return userDao.getUser(id);
    }

    @Override
    public User getByEmail(String email) {
        try {
            return userDao.getUserByEmail(email);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String getPasswordByUserId(long id) {
        return userDao.getUserPassword(id);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Transactional
    @Override
    public void deleteUser(long id) {
        roleDao.removeAllRolesOfUser(id);
        userDao.deleteUser(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        userDao.deleteAll();
    }

    @Transactional
    @Override
    public void addUser(User user) {
        userDao.addUser(user);
        roleDao.addAllRolesOfUser(user);
    }

    @Transactional
    @Override
    public void updateUser(User user) {

        List<Long> lostRoles = new ArrayList<>();
        List<Long> gainedRoles = new ArrayList<>();

        List<Long> rolesOfUserOldVersion = roleDao.getAllRolesIdsOfUser(user.getId());
        List<Long> rolesOfUserNewVersion = user.getRoles().stream().map(Role::getId).collect(Collectors.toList());
        Set<Long> allRolesIds = new HashSet<>();
        allRolesIds.addAll(rolesOfUserNewVersion);
        allRolesIds.addAll(rolesOfUserOldVersion);

        for (Long roleId : allRolesIds) {
            if (rolesOfUserOldVersion.contains(roleId) && !rolesOfUserNewVersion.contains(roleId)) {
                // user lost this role
                lostRoles.add(roleId);
            } else if (!rolesOfUserOldVersion.contains(roleId) && rolesOfUserNewVersion.contains(roleId)) {
                // user gained this role
                gainedRoles.add(roleId);
            }
        }

        roleDao.addRolesToUser(user.getId(), gainedRoles);
        roleDao.removeRolesOfUser(user.getId(), lostRoles);

        userDao.updateUser(user);

    }


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userDao.getUserByEmail(s);
    }
}
