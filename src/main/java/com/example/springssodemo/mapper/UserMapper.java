package com.example.springssodemo.mapper;

import com.example.springssodemo.dto.UserDto;
import com.example.springssodemo.model.Role;
import com.example.springssodemo.model.User;
import com.example.springssodemo.service.role.RoleService;
import com.example.springssodemo.service.role.RoleServiceImpl;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Mapper(componentModel = "spring", uses = RoleServiceImpl.class)
public abstract class UserMapper {

    @Autowired
    private RoleService roleService;

    public abstract User dtoToUser(UserDto userDto);

    public Set<Role> roleIdsListToSet(List<Long> roleIds) {
        return new HashSet<>(roleService.getRolesByIds(roleIds));
    }

}
