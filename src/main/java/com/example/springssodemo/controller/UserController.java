package com.example.springssodemo.controller;


import com.example.springssodemo.model.User;
import com.example.springssodemo.service.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public String userPage(Principal principal, Model model) {
        User user = userService.getByEmail(principal.getName());
        boolean isAdmin = user.getRoles().stream().anyMatch(x -> x.getName().equals("ROLE_ADMIN"));

        model.addAttribute("currentUser", user);
        model.addAttribute("isAdmin", isAdmin);

        return "userPage";
    }

}
