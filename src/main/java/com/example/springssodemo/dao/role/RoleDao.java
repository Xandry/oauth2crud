package com.example.springssodemo.dao.role;


import com.example.springssodemo.model.Role;
import com.example.springssodemo.model.User;

import java.util.List;

public interface RoleDao {
    List<Role> getAllRoles();

    void update(Role role);

    Role getByName(String name);

    void delete(long id);

    void add(Role role);

    void addAllRolesOfUser(User user);

    void removeAllRolesOfUser(long id);

    void removeRolesOfUser(long userId, List<Long> rolesId);

    void addRolesToUser(long userId, List<Long> rolesIds);

    List<Long> getAllRolesIdsOfUser(long userId);

    List<Role> getRolesByIds(List<Long> ids);
}
