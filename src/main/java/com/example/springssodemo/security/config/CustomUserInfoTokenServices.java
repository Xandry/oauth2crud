package com.example.springssodemo.security.config;

import com.example.springssodemo.model.Role;
import com.example.springssodemo.model.User;
import com.example.springssodemo.service.role.RoleService;
import com.example.springssodemo.service.user.UserService;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedPrincipalExtractor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.util.Assert;

import java.util.*;


@Getter
@Setter
@NoArgsConstructor
@Slf4j
public class CustomUserInfoTokenServices implements ResourceServerTokenServices {
    private String userInfoEndpointUrl;
    private String clientId;
    private OAuth2RestOperations restOperations;
    private String tokenType = "Bearer";
    private PrincipalExtractor principalExtractor = new FixedPrincipalExtractor();

    private UserService userService;
    private RoleService roleService;
    private PasswordEncoder passwordEncoder;

    public CustomUserInfoTokenServices(String userInfoEndpointUrl, String clientId) {
        this.userInfoEndpointUrl = userInfoEndpointUrl;
        this.clientId = clientId;
    }


    public void setPrincipalExtractor(PrincipalExtractor principalExtractor) {
        Assert.notNull(principalExtractor, "PrincipalExtractor must not be null");
        this.principalExtractor = principalExtractor;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String accessToken)
            throws AuthenticationException, InvalidTokenException {
        Map<String, Object> map = getMap(this.userInfoEndpointUrl, accessToken);

        if (map.containsKey("sub")) {
            String googleUsername = (String) map.get("name");

            User user = userService.getByEmail(googleUsername);

            if (user == null) {
                user = new User();

                HashSet<Role> roles = new HashSet<>();
                Role userRole = roleService.getByName("ROLE_USER");
                roles.add(userRole);
                user.setRoles(roles);

                user.setEmail(googleUsername);
                user.setPassword(passwordEncoder.encode("12345"));

                userService.addUser(user);
            }

        }

        if (map.containsKey("error")) {
            throw new InvalidTokenException(accessToken);
        }
        return extractAuthentication(map);
    }

    private OAuth2Authentication extractAuthentication(Map<String, Object> map) {
        Object principal = getPrincipal(map);
        List<GrantedAuthority> authorities = new ArrayList<>(userService.getByEmail(principal.toString()).getRoles());
        OAuth2Request request = new OAuth2Request(null, this.clientId, null, true, null,
                null, null, null, null);
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                principal, "N/A", authorities);
        token.setDetails(map);
        return new OAuth2Authentication(request, token);
    }

    /**
     * Return the principal that should be used for the token. The default implementation
     * delegates to the {@link PrincipalExtractor}.
     *
     * @param map the source map
     * @return the principal or {@literal "unknown"}
     */
    protected Object getPrincipal(Map<String, Object> map) {
        Object principal = this.principalExtractor.extractPrincipal(map);
        return (principal == null ? "unknown" : principal);
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        throw new UnsupportedOperationException("Not supported: read access token");
    }

    @SuppressWarnings({"unchecked"})
    private Map<String, Object> getMap(String path, String accessToken) {
        try {
            OAuth2RestOperations oAuth2RestOperations = this.restOperations;
            if (oAuth2RestOperations == null) {
                BaseOAuth2ProtectedResourceDetails resource = new BaseOAuth2ProtectedResourceDetails();
                resource.setClientId(this.clientId);
                oAuth2RestOperations = new OAuth2RestTemplate(resource);
            }
            OAuth2AccessToken existingToken = oAuth2RestOperations.getOAuth2ClientContext()
                    .getAccessToken();
            if (existingToken == null || !accessToken.equals(existingToken.getValue())) {
                DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(
                        accessToken);
                token.setTokenType(this.tokenType);
                oAuth2RestOperations.getOAuth2ClientContext().setAccessToken(token);
            }
            return oAuth2RestOperations.getForEntity(path, Map.class).getBody();
        } catch (Exception ex) {
            return Collections.singletonMap("error",
                    "Could not fetch user details");
        }
    }
}
