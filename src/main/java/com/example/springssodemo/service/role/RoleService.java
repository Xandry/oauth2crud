package com.example.springssodemo.service.role;


import com.example.springssodemo.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();

    Role getByName(String name);

    List<Role> getRolesByIds(List<Long> ids);

    void create(Role role);

    void delete(long id);

    void update(Role role);
}
