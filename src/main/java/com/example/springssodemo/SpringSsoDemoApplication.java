package com.example.springssodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSsoDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSsoDemoApplication.class, args);
    }

}
